# Half-Life Sequences Extractor

## Description

Small tool to extract list of sequences from Half-Life .mdl file in the format fitting for using in Jackhammer editor.

## Dependencies

No specific dependencies other than standard C library.

## How to build

To keep it simple there is no Makefile. Just one small shell script called compile.sh

    chmod +x compile.sh
    ./compile.sh
	
On Windows you can use .bat file (assumed MinGW is installed and its bin/ directory is in the PATH environment variable):

	compile.bat

You may want to rewrite shell script to set some other arguments to compiler.
