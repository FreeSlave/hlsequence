
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "studio.h"

void printHelp(const char* program)
{
    fprintf(stdout, "Usage:\n  %s <input mdl file> [options]\n", program);
    fprintf(stdout, "Options:\n");
    fprintf(stdout, "  -o outfile  write to outfile instead of standard output");
    fprintf(stdout, "  -n number   default value for sequence index. By default 0 will be used\n");
    fprintf(stdout, "  -s name     name of default sequence. Program will attempt to find corresponding sequence index\n");
    fprintf(stdout, "              Note that -n and -s can not be used together. Also note that sequence name may be not unique per model\n");
    fprintf(stdout, "  -h          show this message\n");
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "Error: no arguments\n");
        fprintf(stderr, "Type '%s -h' to get help\n", argv[0]);
        return EXIT_FAILURE;
    }
    
    const char* outFileName = NULL;
    int seqNum = -1;
    const char* seqName = NULL;
    const char* fileName = NULL;
    
    int argi;
    for (argi = 1; argi < argc; ++argi)
    {
        const char* opt = argv[argi];
        if (strcmp(opt, "-o") == 0)
        {
            argi++;
            if (argi >= argc)
            {
                fprintf(stderr, "File name expected after -o\n");
                return EXIT_FAILURE;
            }
            opt = argv[argi];
            if (outFileName)
            {
                fprintf(stderr, "Name of output file is already set to %s\n", outFileName);
                return EXIT_FAILURE;
            }
            outFileName = opt;
        }
        else if (strcmp(opt, "-n") == 0)
        {
            argi++;
            if (argi >= argc)
            {
                fprintf(stderr, "Sequence index expected after -n\n");
                return EXIT_FAILURE;
            }
            opt = argv[argi];
            if (seqNum != -1)
            {
                fprintf(stderr, "Sequence index is already set to %d\n", seqNum);
                return EXIT_FAILURE;
            }
            if (seqName)
            {
                fprintf(stderr, "Sequence index and sequence name can't be set together\n");
                return EXIT_FAILURE;
            }
            seqNum = strtol(opt, NULL, 10);
        }
        else if (strcmp(opt, "-s") == 0)
        {
            argi++;
            if (argi >= argc)
            {
                fprintf(stderr, "Sequence name expected after -s\n");
                return EXIT_FAILURE;
            }
            opt = argv[argi];
            if (seqName)
            {
                fprintf(stderr, "Sequence name is already set to %s\n", seqName);
                return EXIT_FAILURE;
            }
            if (seqNum != -1)
            {
                fprintf(stderr, "Sequence index and sequence name can't be set together\n");
                return EXIT_FAILURE;
            }
            seqName = opt;
        }
        else if (strcmp(opt, "-h") == 0)
        {
            printHelp(argv[0]);
            return EXIT_SUCCESS;
        }
        else
        {
            if (fileName)
            {
                fprintf(stderr, "Input file is already set to %s\n", fileName);
                return EXIT_FAILURE;
            }
            fileName = opt;
        }
    }
    
    if (!fileName)
    {
        fprintf(stderr, "Input file is not specifed\n");
        return EXIT_FAILURE;
    }
    
    if (seqNum == -1)
        seqNum = 0;
    
    FILE* file = fopen(fileName, "rb");
    if (!file)
    {
        fprintf(stderr, "Could not open file %s\n", fileName);
        return EXIT_FAILURE;
    }
    
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fseek(file, 0, SEEK_SET);
    
    void* buffer = malloc(size);
    if (!buffer)
    {
        fprintf(stderr, "Could not allocate enough memory\n");
        fclose (file);
        return EXIT_FAILURE;
    }
    
    if (fread(buffer, size, 1, file) != 1)
    {
        fprintf(stderr, "Could not read the whole file\n");
        fclose(file);
        free(buffer);
        return EXIT_FAILURE;
    }
    fclose(file);
    
    if (size < sizeof(studiohdr_t))
    {
        fprintf(stderr, "File %s is not a .mdl or is corrupted\n", fileName);
        free(buffer);
        return EXIT_FAILURE;
    }
    
    if (strncmp ((const char*)buffer, "IDST", 4) != 0 && strncmp ((const char*)buffer, "IDSQ", 4) != 0)
    {
        fprintf(stderr, "File %s does not have valid .mdl id\n", fileName);
        free (buffer);
        return EXIT_FAILURE;
    }
    
    studiohdr_t* header = (studiohdr_t*)buffer;

    FILE* out = stdout;
    
    if (outFileName)
    {
        FILE* outFile = fopen(outFileName, "w");
        if (!outFile)
        {
            fprintf(stderr, "Could not open file %s\n", outFileName);
            return EXIT_FAILURE;
        }
        out = outFile;
    }
    
    int i;
    if (seqName)
    {
        for (i=0; i<header->numseq; ++i)
        {
            mstudioseqdesc_t* sequence = (mstudioseqdesc_t*)((byte *)buffer + header->seqindex) + i;
            if (strcmp(seqName, sequence->label) == 0)
            {
                seqNum = i;
                break;
            }
        }
    }
    
    if (seqNum >= header->numseq || seqNum < 0)
        seqNum = 0;
    
    fprintf(out, "\tsequence(Choices) : \"Animation Sequence (editor)\" : %d =\n", seqNum);
    fprintf(out, "\t[\n");
    
    for (i=0; i<header->numseq; ++i)
    {
        mstudioseqdesc_t* sequence = (mstudioseqdesc_t*)((byte *)buffer + header->seqindex) + i;
        fprintf(out, "\t\t%d : \"%s\"\n", i, sequence->label);
    }
    fprintf(out, "\t]\n");
    
    if (out != stdout)
        fclose(out);
    
    free(buffer);
    return EXIT_SUCCESS;
}
